FROM openjdk:8-jdk-alpine

COPY ./target/CQRS-Axon-*.jar app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
